# rshell

A remote shell exec application on windows platform

# Online Help

[https://xied5531.gitee.io/rshell/](https://xied5531.gitee.io/rshell/)

## Main Features
- Run shell commands on the group of remote linux systems from local Windows platform
- Commands can be one or more command split by ; 
- Target systems can be group or all or single
- User can be username or auto change root
- Upload/Download files to/from target linux systems
- Commands can auto complete

## Environment
- Windows
- Python 3.x

If want auto compelte, install pyreadline, ```pip install pyreadline```

## hosts/xxx.ini

> Use ini format, the format is ipaddress = hostname.

```
[group1]
192.145.18.132 = host-0001
192.145.18.133 = host-0002

[group2]
192.168.0.1 = host1
192.168.0.2 = host2
192.168.0.3 = host3

```

## passwds/xxx.ini

> Use ini format, the keyword default/username/passwd/rootpass can not change. the format is key = value.

```
[default]
username = aaa_user
passwd = aaa_pass
rootpass = root_pass
```

## Build exe

```
pyinstaller --clean --onefile --nowindowed --version-file file_version_info.txt --icon=rshell.ico rshell.py
```

## Run exe

```
$ dir

2018/05/08  18:50    <DIR>          .
2018/05/08  18:50    <DIR>          ..
2018/05/08  16:36    <DIR>          hosts
2018/05/08  16:36    <DIR>          passwds
2018/05/08  20:19         6,091,956 rshell.exe
2018/05/08  12:42    <DIR>          scripts
```

## Troubleshooting

- Error Info: *** invalid number of arguments
> solution：Check the help

- Auto-compelte for hostgroup not work
> solution：Make sure the pyreadline module installed

- Error Info: Do remote shell failed.
> solution：Check plink installed and can run in cmd.

> solution：Check the command or script run correctly on target host manually.

- Error Info: Do remote copy failed.
> solution：Check pscp installed and can run in cmd.

> solution：Check the target file existed.

> solution：Check the target file right.

- Other Error Info
> solution：Check Python version is 3.x
