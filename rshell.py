#!/usr/bin/env python
# -*- coding:utf-8 -*-

import shutil
import glob
import logging
import json
import os
import sys
import cmd
import tempfile
import subprocess
import pprint 
import ipaddress
import ctypes
import multiprocessing
from multiprocessing import cpu_count
from concurrent.futures import ProcessPoolExecutor
from configparser import SafeConfigParser

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('RSHELL')

def checkCurrentDir(cdir):
    iniDirs = [f for f in os.listdir(cdir)]
    if 'hosts' in iniDirs and 'passwds' in iniDirs and 'scripts' in iniDirs:
        if os.path.isfile(os.path.join(cdir, 'hosts', 'default.ini')) and os.path.isfile(os.path.join(cdir, 'passwds', 'default.ini')) and os.path.isfile(os.path.join(cdir, 'scripts', 'default.sh')):
            return True
    return False

exeCurrentDir=os.path.dirname(sys.executable)
osCurrentDir=os.path.split(os.path.realpath(__file__))[0]
currentDir = None
if checkCurrentDir(exeCurrentDir):
    currentDir = exeCurrentDir
elif checkCurrentDir(osCurrentDir):
    currentDir = osCurrentDir
else:
    if exeCurrentDir != osCurrentDir:
        print(exeCurrentDir + ' or ' + osCurrentDir + ' do not have right files[hosts/default.ini,passwds/default.ini,scripts/default.sh]')
    else:
        print(exeCurrentDir + ' do not have right files[hosts/default.ini,passwds/default.ini,scripts/default.sh]')
    sys.exit(1)

def checkExe(e):
    try:
        if shutil.which(e):
            return True
    except:
        return False
    return False

def checkExeFile(ef):
    if os.path.isfile(os.path.join(currentDir, 'tools', ef)):
        return True
    return False

plink = ''
if not checkExe('plink'):
    if not checkExeFile('plink.exe'):
        print('plink command not found in win env set or plink.exe not found in tools directory of ' + currentDir)
        sys.exit(1)
    else:
        plink = 'tools\plink.exe'
else:
    plink = 'plink'
pscp = ''
if not checkExe('pscp'):
    if not checkExeFile('pscp.exe'):
        print('pscp command not found in win env set or pscp.exe not found in tools directory of ' + currentDir)
        sys.exit(1)
    else:
        pscp = 'tools\pscp.exe'
else:
    pscp = 'pscp'

hostsParser = None
hostgroups = []

passwdsParser = None
passwds = []
username = ''
passwd = ''
rootpass = ''
scripts = []

hostfile = ''
hostfiles = []
passwdfile = ''
passwdfiles = []

class ColorPrint:
    stdoutHandle = ctypes.windll.kernel32.GetStdHandle(-11)

    def setCmdColor(self, color, handle=stdoutHandle):
        ctypes.windll.kernel32.SetConsoleTextAttribute(handle, color)

    def resetCmdColor(self):
        self.setCmdColor(0x04 | 0x02 | 0x01)

    def red(self, text):
        self.setCmdColor(0x04 | 0x08)
        print(text)
        self.resetCmdColor()

    def green(self, text):
        self.setCmdColor(0x02 | 0x08)
        print(text)
        self.resetCmdColor()

    def blue(self, text):
        self.setCmdColor(0x01 | 0x08)
        print(text)
        self.resetCmdColor()

    def redWithBlue(self, text):
        self.setCmdColor(0x04 | 0x08| 0x10 | 0x80)
        print(text)
        self.resetCmdColor()   

def loadHosts(hfile='default.ini'):
    global hostsParser
    global hostgroups
    global hostfile
    global hostfiles
    hostsParser = SafeConfigParser()
    hostgroups = []    
    hostfile = os.path.join(currentDir, 'hosts', hfile)
    hostfiles = [f for f in os.listdir(os.path.join(currentDir, 'hosts'))]
    if os.path.isfile(hostfile):
        try:            
            hostsParser.read(hostfile)
            hostgroups.extend(hostsParser.sections())
        except Exception as e:
            logger.error(hostfile + ' error: ' + str(e))
    else:
        logger.error(hostfile + ' not exist.')

def loadPasswds(pfile='default.ini'):
    global passwdsParser
    global passwds
    global username
    global passwd
    global rootpass
    global passwdfile
    global passwdfiles
    passwdsParser = SafeConfigParser()
    passwds = []
    username = ''
    passwd = ''
    rootpass = ''
    passwdfile = os.path.join(currentDir, 'passwds', pfile)
    passwdfiles = [f for f in os.listdir(os.path.join(currentDir, 'passwds'))]
    if os.path.isfile(passwdfile):
        try:
            passwdsParser.read(passwdfile)
            passwds.extend(passwdsParser.sections())
            username = passwdsParser.get('default', 'username')
            passwd = passwdsParser.get('default', 'passwd')
            rootpass = passwdsParser.get('default', 'rootpass')
        except Exception as e:
            logger.error(passwdfile + ' error: ' + str(e))
    else:
        logger.error(passwdfile + ' not exist.')

def loadScripts():
    global scripts
    scripts = []
    scripts.extend([f for f in os.listdir(os.path.join(currentDir, 'scripts')) if os.path.isfile(os.path.join(currentDir, 'scripts', f))])

def reload():
    if hostfile != '':
        loadHosts(hostfile)
    else:
        loadHosts()
    if passwdfile != '':
        loadPasswds(passwdfile)
    else:
        loadPasswds()
    loadScripts()

def checkIpFormat(hostgroup):
    try:
        ipaddress.ip_address(hostgroup)
        return True
    except ValueError:
        return False

def checkHostgroup(hostgroup):
    if hostgroup == 'all' or hostgroup in hostgroups:
        return True
    elif checkIpFormat(hostgroup):
        return True
    else:
        logger.error(hostgroup + ' not in group: ' + ' '.join(hostgroups) + ' or all or ip format wrong.')
        return False

def checkPasswdgroup(passwdgroup):
    if passwdgroup in passwds:
        return True
    else:
        logger.error(passwdgroup + ' not in group: ' + passwds)
        return False

def checkScript(script):
    if script in scripts:
        return True
    else:
        logger.error(script + ' not in the right scripts list, see dir scripts first.')
        return False

def mpp(funcs=[]):
    if not funcs:
        return
    funcName = funcs[0]['name']
    funcArgs = [ a['args'] for a in funcs ]
    with ProcessPoolExecutor(max_workers=cpu_count()*10) as executor:
        try:
            for result in executor.map(funcName, funcArgs):
                ip = result['ip']
                hostname = result['hostname']
                output = result['output']
                print('')
                cprint.green(ip + ' | ' + hostname + ' | >>>>>>')
                if 'DO REMOTE SHELL FAILED.' in output or 'DO REMOTE COPY FAILED.' in output:
                    cprint.red('\n'.join(output))
                else:
                    print('\n'.join(output))
                print('')
        except KeyboardInterrupt:
            executor.shutdown(wait=False)

def mkPlink(plinkArgs):
    ip, hostname, user, passwd, cmdfile = plinkArgs
    output = {}
    output['ip'] = ip
    output['hostname'] = hostname
    cmdline = 'echo y | ' + plink + ' -l ' + user + ' -pw ' + passwd + ' -m ' + cmdfile + ' ' + ip
    logger.debug(cmdline)
    try:
        output['output'] = subprocess.check_output(
            cmdline,
            shell=True,
            stderr=subprocess.STDOUT,
        ).decode('utf-8').splitlines()
    except subprocess.CalledProcessError as err:
        logger.debug(err.output)
        output['output'] = ['DO REMOTE SHELL FAILED.', str(err.output)]
    return output

def mkCmdFileRoot(rpass, cmdargs):
    logger.debug(cmdargs)
    tf = tempfile.NamedTemporaryFile(prefix="rshell_", delete=False)
    with open(tf.name, 'w') as f:
        f.write('#!/bin/bash\n')
        f.write('sudo -kS bash - << EOF\n')
        f.write(rpass + '\n')
        f.writelines("\n".join(cmdargs.split(';')))
        f.write('\nEOF\n')
    logger.debug(tf.name)
    return tf.name

def mkCmdFile(cmdargs):
    logger.debug(cmdargs)
    tf = tempfile.NamedTemporaryFile(prefix="rshell_", delete=False)
    with open(tf.name, 'w') as f:
        f.writelines("\n".join(cmdargs.split(';')))
    logger.debug(tf.name)
    return tf.name

def mkPlinks(iphosts, cmdfile):
    funcs = []
    for ip, hostname in iphosts:
        p = {}
        p['name'] = mkPlink
        p['args'] = (ip, hostname, username, passwd, cmdfile)
        funcs.append(p)
    return funcs

def mkPscp(pscpArgs):
    ip, hostname, user, passwd, ptype, file1, file2 = pscpArgs
    output = {}
    output['ip'] = ip
    output['hostname'] = hostname
    if ptype == 'download':
        if not os.path.exists(file2 + os.sep + ip):
            os.makedirs(file2 + os.sep + ip)
        cmdline = 'echo y | ' + pscp + ' -l ' + user + ' -pw ' + passwd + ' ' + ip + ':' + file1 + ' ' + file2 + os.sep + ip
    elif ptype == 'upload':
        cmdline = 'echo y | ' + pscp + ' -l ' + user + ' -pw ' + passwd + ' ' + file1 + ' ' + ip + ':' + file2
    logger.debug(cmdline)
    try:
        output['output'] = subprocess.check_output(
            cmdline,
            shell=True,
            stderr=subprocess.STDOUT,
        ).decode('utf-8').splitlines()
    except subprocess.CalledProcessError as err:
        logger.debug(err.output)
        output['output'] = ['DO REMOTE COPY FAILED.', str(err.output)]
    return output

def mkPscps(iphosts, ptype, file1, file2):
    funcs = []
    for ip, hostname in iphosts:
        p = {}
        p['name'] = mkPscp
        p['args'] = (ip, hostname, username, passwd, ptype, file1, file2)
        funcs.append(p)
    return funcs

def mkIpHosts(hostgroupname):
    if hostgroupname == 'all':
        iphosts = []
        for section in hostsParser.sections():
            iphosts.extend(map(lambda x: (x[0], section + '->' + x[1]),hostsParser.items(section)))
        return iphosts
    elif checkIpFormat(hostgroupname):
        iphosts = []
        for section in hostsParser.sections():
            iphosts.extend(map(lambda x: (x[0], section + '->' + x[1]),hostsParser.items(section)))
        logger.debug(iphosts)
        result = list(filter(lambda x: x[0] == hostgroupname, iphosts))
        logger.debug(result)
        if not result:
            logger.error(hostgroupname + ' not in hosts list.')
        return result
    else:
        return hostsParser.items(hostgroupname)

COMMANDS = ['awk','bzip2','cat','cd','chgrp','chmod','chown','clear','cp','crontab','date','df','diff','du','export','find','free','ftp','grep','gzip','halt','head','ifconfig','kill','killall','ln','locate','ls','mkdir','mount','mv','nslookup','passwd','ping','ps','pwd','reboot','rm','rmdir','rpm','service','shutdown','sort','ssh','tail','tar','telnet','top','umount','uname','unzip','wget','whatis','whereis']
def complete_hostgroup_command(text, line):
    completions = []
    num = len(line.split())
    if num == 1:
        if not text:
            completions = hostgroups[:]
    elif num == 2:
        if text:
            completions = [ f for f in hostgroups if f.startswith(text)]
        else:
            completions = COMMANDS
    else:
        if text:
            completions = [ f for f in COMMANDS if f.startswith(text)]
        else:
            completions = COMMANDS
    return completions

def complete_hostgroup_script(text, line):
    completions = []
    num = len(line.split())
    if num == 1:
        if not text:
            completions = hostgroups[:]
    elif num == 2:
        if text:
            completions = [ f for f in hostgroups if f.startswith(text)]
        else:
            completions = scripts
    elif num == 3:
        if text:
            completions = [ f for f in scripts if f.startswith(text)]
    return completions

def complete_hostgroup(text, line):
    completions = []
    num = len(line.split())
    if num == 1:
        if not text:
            completions = hostgroups[:]
    elif num == 2:
        if text:
            completions = [ f for f in hostgroups if f.startswith(text)]
    return completions

def complete_hosts(text, line):
    completions = []
    num = len(line.split())
    if num == 1:
        if not text:
            completions = hostfiles[:]
    elif num == 2:
        if text:
            completions = [ f for f in hostfiles if f.startswith(text)]
    return completions

def complete_passwds(text, line):
    completions = []
    num = len(line.split())
    if num == 1:
        if not text:
            completions = passwdfiles[:]
    elif num == 2:
        if text:
            completions = [ f for f in passwdfiles if f.startswith(text)]
    return completions

def showEnv():
    print('Plink Path:     ' + plink)
    print('Pscp Path:      ' + pscp)
    print('Work Directory: ' + currentDir)
    print('Host File:      ' + hostfile)
    print('Passwd File:    ' + passwdfile)
    print()

class Rshell(cmd.Cmd):
    """A Remote Shell Application."""
    prompt = 'rshell: '
    intro = '''
  ______     ______     __  __     ______     __         __        
 /\  == \   /\  ___\   /\ \_\ \   /\  ___\   /\ \       /\ \       
 \ \  __<   \ \___  \  \ \  __ \  \ \  __\   \ \ \____  \ \ \____  
  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_____\  \ \_____\  \ \_____\ 
   \/_/ /_/   \/_____/   \/_/\/_/   \/_____/   \/_____/   \/_____/ 
 ------ A Remote Shell Exec Application On Windows Platform ------
    '''
    
    def preloop(self):
        reload()

    def do_show(self, line):
        """show
        Show the informations of current context.
        EXAMPLE: show"""
        showEnv()

    def do_refresh(self, line):
        """refresh
        Refresh the file list in hosts and passwds and scripts directory.
        EXAMPLE: refresh"""
        global hostfiles
        hostfiles = [f for f in os.listdir(os.path.join(currentDir, 'hosts'))]
        global passwdfiles
        passwdfiles = [f for f in os.listdir(os.path.join(currentDir, 'passwds'))]
        loadScripts()

    def complete_loadhosts(self, text, line, begidx, endidx):
        return complete_hosts(text, line)

    def do_loadhosts(self, hfile):
        """loadhosts [hostfile]
        Load hosts/xxx.ini file and use the info.
        EXAMPLE: loadhosts xxx.ini"""
        loadHosts(hfile)

    def complete_loadpasswds(self, text, line, begidx, endidx):
        return complete_passwds(text, line)

    def do_loadpasswds(self, pfile):
        """loadpasswds [passwdfile]
        Load passwds/xxx.ini file and use the info.
        EXAMPLE: loadpasswds xxx.ini"""
        loadPasswds(pfile)

    def complete_sudo(self, text, line, begidx, endidx):
        return complete_hostgroup_command(text, line)

    def do_sudo(self, line):
        """sudo [hostgroup] [commands]
        Run commands on hostgroup in hosts/xxx.ini use default username and passwd and sudo with rootpass in passwds/xxx.ini
        EXAMPLE: sudo group1 date; whoami; df -h; ls -l"""
        args = line.split()
        if len(args) < 2:
            logger.error('*** invalid number of arguments')
            return
        hostgroupname = args[0]
        cmdargs = ' '.join(args[1:])
        if checkHostgroup(hostgroupname):
            mpp(mkPlinks(mkIpHosts(hostgroupname), mkCmdFileRoot(rootpass, cmdargs)))

    def complete_do(self, text, line, begidx, endidx):
        return complete_hostgroup_command(text, line)

    def do_do(self, line):
        """do [hostgroup] [commands]
        Run commands on hostgroup in hosts/xxx.ini use default username and passwd in passwds/xxx.ini
        EXAMPLE: do group1 date; whoami; df -h; ls -l"""
        args = line.split()
        if len(args) < 2:
            logger.error('*** invalid number of arguments')
            return
        logger.debug(args)
        hostgroupname = args[0]
        cmdargs = ' '.join(args[1:])
        if checkHostgroup(hostgroupname):
            mpp(mkPlinks(mkIpHosts(hostgroupname), mkCmdFile(cmdargs)))

    def complete_script(self, text, line, begidx, endidx):
        return complete_hostgroup_script(text, line)

    def do_script(self, line):
        """script [hostgroup] [scriptname]
        Run script in scripts/xxx.xx without args on hostgroup in hosts/xxx.ini use default username and passwd in passwds/xxx.ini
        EXAMPLE: script group1 test.sh"""
        args = line.split()
        if len(args) != 2:
            logger.error('*** invalid number of arguments')
            return
        hostgroupname = args[0]
        cmdargs = currentDir + '\\scripts\\' + ' '.join(args[1:])
        scriptfile = args[1]
        if checkHostgroup(hostgroupname) and checkScript(scriptfile):
            mpp(mkPlinks(mkIpHosts(hostgroupname), cmdargs))

    def complete_upload(self, text, line, begidx, endidx):
        return complete_hostgroup(text, line)

    def do_upload(self, line):
        """upload [hostgroup] [localfile] [remotefile/remotedir]
        Upload localfile to hostgroup in hosts/xxx.ini as remotefile or remotedir use default username and passwd in passwds/xxx.ini
        EXAMPLE: upload group1 LICENSE LICENSEASREMOTE"""
        args = line.split()
        if len(args) != 3:
            logger.error('*** invalid number of arguments')
            return
        hostgroupname = args[0]
        if checkHostgroup(hostgroupname):
            mpp(mkPscps(mkIpHosts(hostgroupname), 'upload', args[1], args[2]))

    def complete_download(self, text, line, begidx, endidx):
        return complete_hostgroup(text, line)

    def do_download(self, line):
        """download [hostgroup] [remotefile] [localdir]
        Download remotefile from hostgroup in hosts/xxx.ini to localdir use default username and passwd in passwds/xxx.ini
        EXAMPLE: download group1 LICENSE* tmp"""
        args = line.split()
        if len(args) != 3:
            logger.error('*** invalid number of arguments')
            return
        if not os.path.isdir(args[2]):
            logger.error('Local directory : ' + args[2] + ' not eixst, please create it first.')
            return
        hostgroupname = args[0]
        if checkHostgroup(hostgroupname):
            mpp(mkPscps(mkIpHosts(hostgroupname), 'download', args[1], args[2]))

    def do_EOF(self, line):
        """Clean temp files before exit(Use Ctrl-Z plus Enter to exit)."""
        tempdir = tempfile.gettempdir()
        for f in glob.glob(tempdir + os.sep + 'rshell_*'):
            os.remove(f)
        return True

    def emptyline(self):
        pass

    def mycmdloop(self, intro=None):
        while True:
            try:
                self.cmdloop()
                break
            except KeyboardInterrupt:
                print('')
                self.intro = ''

if __name__ == '__main__':   
    cprint = ColorPrint()
    multiprocessing.freeze_support()
    Rshell().mycmdloop()

